/**
 *
 */
package com.powerdata.framework.base;

/**
 *
 */
public class Query extends Base{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
     * 自动排序属性
     */
    private String order;

    /**
     * 排序方式
     */
    private boolean isAsc;

    /**
     * 跳转
     */
    private int page = 1;

    /**
     * 每页显示记录数
     */
    private int pageSize = 20;

    public Query() {

    }

    public Query(int pageSize) {
        this.pageSize = pageSize;
    }
    
    public String getOrder() {
        return order;
    }

    public void setOrder(String order) {
        this.order = order;
    }

    public boolean getIsAsc() {
        return isAsc;
    }

    public void setIsAsc(boolean isAsc) {
        this.isAsc = isAsc;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

}
