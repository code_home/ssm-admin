package com.powerdata.framework.annotation.cache;

import java.util.concurrent.TimeUnit;

import org.springframework.data.redis.core.RedisTemplate;

import com.powerdata.framework.util.SpringContextHolder;

/**
 * 缓存工具
 * @author he.xx
 */
public class RedisCacheOperateUtil {
	private static RedisCacheOperateUtil redisCacheOperateUtil;
	private static RedisTemplate<String, Object> redisTemplate;
	
	private RedisCacheOperateUtil() {}
	
	public static RedisCacheOperateUtil getInstance() {
		if (redisCacheOperateUtil == null) {
			redisCacheOperateUtil = new RedisCacheOperateUtil();
			redisTemplate = SpringContextHolder.getBean("redisTemplate");
		}
		return redisCacheOperateUtil;
	}
	
	public void put(String key ,Object value ,String []nameSpace ,long expiration){
		redisTemplate.opsForValue().set(key, value, expiration, TimeUnit.SECONDS);
	}
	
	public Object get(String key ,String []nameSpace){
		return redisTemplate.opsForValue().get(key);
	}
	
}
