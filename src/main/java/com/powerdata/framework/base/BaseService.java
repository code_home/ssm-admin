package com.powerdata.framework.base;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public abstract class BaseService {
	protected final Logger LOG = LoggerFactory.getLogger(getClass().getName());
}
