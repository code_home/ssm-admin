package com.powerdata.manage.background.service;

import com.powerdata.manage.background.model.SysConfigEntity;

public interface ISysConfigService {
	/**
	 * 获取总条数
	 */
	public int findPageCount(SysConfigEntity sysConfigEntity);
	
	/**
	 * 保存单条记录
	 */
	public String insert(SysConfigEntity sysConfigEntity);
	
	/**
	 * 更新记录
	 */
	public int update(SysConfigEntity sysConfigEntity);
	
	/**
	 * 通过id删除记录
	 */
	public int remove(String id);
	
	/**
	 * 通过id查询数据
	 */
	public SysConfigEntity getById(String id); 
	
	/**
	 * 保存系统配置的操作日志
	 * @param sysConfigEntity
	 * @param operationType
	 */
	public void saveSysLog(SysConfigEntity sysConfigEntity,String operationType);
	
	public String getValueBykey(String key);
}
