package com.powerdata.manage.background.dao;

import java.util.List;

import com.powerdata.manage.background.model.User;

/**
 * @author he.xx
 * 用户信息，增删改查
 */
public interface IUserMapper {
	
	/**
	 * 查找
	 * @throws Exception
	 */
	public User select(User user);
	
	/**
	 * 新增用户
	 */
	public void insert(User user);
	
	/**
	 * 通过id查询数据
	 */
	public User getById(String id);
	
	/**
	 * 查询用户列表
	 * @return
	 * @throws Exception
	 */
	public List<User> queryList(User user);
	
	public List<User> findPageList(User user);
	
	public int findPageListCount(User user);
	
	/**
	 * 更新用户
	 * @param user
	 * @return
	 * @throws Exception
	 */
	public int update(User user);
	
	/**
	 * 删除用户
	 * @param user
	 * @return
	 * @throws Exception
	 */
	public int delete(String id);
}
