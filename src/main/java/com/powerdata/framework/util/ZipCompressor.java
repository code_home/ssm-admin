package com.powerdata.framework.util;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.CRC32;
import java.util.zip.CheckedOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

/**
 * <p>Title: ZipCompressor</p>
 * <p>Description: </p>
 * @author: zheng.qq
 * @date: 2016年12月26日
 */
public class ZipCompressor {

	private static final byte[] BUFFER = new byte[4096 * 1024];
	
	public static void compress(String zipFilePath, String... pathName) {   
        ZipOutputStream out = null;     
        try {    
            FileOutputStream fileOutputStream = new FileOutputStream(zipFilePath);     
            CheckedOutputStream cos = new CheckedOutputStream(fileOutputStream,     
                    new CRC32());     
            out = new ZipOutputStream(cos);     
            for (int i=0;i<pathName.length;i++){  
                compress(new File(pathName[i]), out, "");     
            }  
            out.close();    
        } catch (Exception e) {     
            throw new RuntimeException(e);     
        }   
    }     
    
	
	private static void compress(File file, ZipOutputStream out, String baseDir) {     
        /* 判断是目录还是文件 */    
        if (file.isDirectory()) {     
            System.out.println("压缩：" + baseDir + file.getName());     
            compressDirectory(file, out, baseDir);     
        } else {     
            System.out.println("压缩：" + baseDir + file.getName());     
            compressFile(file, out, baseDir);     
        }     
    }     
	 
	 /** 压缩一个目录 */    
    private static void compressDirectory(File dir, ZipOutputStream out, String baseDir) {     
        if (!dir.exists())     
            return;     
    
        File[] files = dir.listFiles();     
        for (int i = 0; i < files.length; i++) {     
            /* 递归 */    
            compress(files[i], out, baseDir + dir.getName() + "/");     
        }     
    }
    
    /** 压缩一个文件 */    
    private static void compressFile(File file, ZipOutputStream out, String baseDir) {     
        if (!file.exists()) {     
            return;     
        }     
        try {     
            BufferedInputStream bis = new BufferedInputStream(     
                    new FileInputStream(file));     
            ZipEntry entry = new ZipEntry(baseDir + file.getName());     
            out.putNextEntry(entry);     
            int count;       
            while ((count = bis.read(BUFFER, 0, BUFFER.length)) != -1) {     
                out.write(BUFFER, 0, count);     
            }     
            bis.close();     
        } catch (Exception e) {     
            throw new RuntimeException(e);     
        }     
    }     

    /** 按流压缩 
     * @throws IOException */    
    public static void compressFile(String filePath, InputStream in, ZipOutputStream out) throws IOException {     
    	BufferedInputStream bis = new BufferedInputStream(in);     
        ZipEntry entry = new ZipEntry(filePath);     
        out.putNextEntry(entry);     
        int count;   
        while ((count = bis.read(BUFFER, 0, BUFFER.length)) != -1) {     
            out.write(BUFFER, 0, count);     
        }     
        bis.close();     
    } 
    
    public static Map<String, Date> readZipFile(String file) throws Exception {  
    	Map<String, Date> map = new HashMap<String, Date>();
    	ZipFile zf = null;
    	try{
    		zf = new ZipFile(file);  
    		Enumeration<? extends ZipEntry> en = zf.entries();  
            while (en.hasMoreElements()) {  
                ZipEntry ze = en.nextElement();  
                if (!ze.isDirectory()) {  
                	String name = ze.getName();
                	Date date = new Date(ze.getTime());
                	map.put(name, date);
                }
            }  
    	}catch(Exception e){
    		System.err.println("压缩文件已损坏！无法打开！");
    	}finally{
    		if(zf!=null)
    			zf.close();
    	}
        return map;
    }  
    
    public static boolean checkZipGood(String filePath) throws IOException{
    	boolean rs = true;
    	ZipFile zf = null;
    	try{
    		zf = new ZipFile(filePath);  
    	}catch(Exception e){
    		System.err.println("临时文件("+filePath+")无法打开，将删除！");
    		File file = new File(filePath);
        	if(file.exists()){
        		file.delete();
        	}
    		rs = false;
    	}finally{
    		if(zf != null)
    			zf.close();
    	}
    	return rs;
    	
    }
    
    
    public static void main(String[] args) throws Exception {     
//        compress("E:/logs/c.zip", "E:/logs/cds","E:/logs/version-2");
//    	List<String> addItemList = new ArrayList<String>();
//    	addItemList.add("C:\\Users\\QIAO-OS\\Desktop\\cds-sql\\tbl_collection_order.sql");
//    	List<String> deleteItemList = new ArrayList<String>();
//    	deleteItemList.add("lh/Mckz/2TD20CM2/ABL2TD20DU1CM2//1.JPG");
//    	modified("E:\\data\\8c244dc9ec9133f2e99607238fe689b0.zip",addItemList, deleteItemList);
    	File f = new File("E:\\data\\4.zip");
    	String fpath = f.getParent();
    	String fname = f.getName();
    	String fname2 = "tmp_" + fname;
    	String file2 = fpath + File.separator + fname2;
    }  

}
