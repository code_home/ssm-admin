package com.powerdata.paltform.ftp.service.impl;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;

import javax.annotation.Resource;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.integration.Message;
import org.springframework.integration.MessageChannel;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.stereotype.Service;

import com.powerdata.framework.util.FileUtil;
import com.powerdata.paltform.ftp.service.IFTPService;

@Service
public class FTPServiceImpl implements IFTPService {
	private static final Logger LOG = Logger.getLogger(FTPServiceImpl.class);
	
	@Resource
	private MessageChannel ftpUploadChannel;
	@Resource
	private MessageChannel ftpRemoveChannel;

	@Override
	public boolean uploadFile(String ftpServerPath, String fileName, InputStream in) throws Exception {
		if (StringUtils.isNotBlank(ftpServerPath) && ftpServerPath.startsWith("/")) {
			ftpServerPath = StringUtils.substringAfter(ftpServerPath, "/");
		}
		ftpServerPath = FileUtil.getRealFilePath(ftpServerPath, "linux");
		Message<byte[]> message=null;
		
		boolean bool = false;
		try {
			message = MessageBuilder.withPayload(IOUtils.toByteArray(in))
					.setHeader("remote_dir", new String(ftpServerPath.getBytes(Charset.forName("UTF-8")),"ISO-8859-1"))
					.setHeader("remote_filename", new String(fileName.getBytes(Charset.forName("UTF-8")),"ISO-8859-1")).build();
			bool = ftpUploadChannel.send(message);
		} catch (IOException e) {
			LOG.error("ftp上传图片产生异常",e);
			throw new Exception(e);
		} finally{
			IOUtils.closeQuietly(in);
		}
		return bool;
	}

	@Override
	public boolean deleteFile(String fileName, String ftpServerPath) throws Exception {
		if (StringUtils.isNotBlank(ftpServerPath) && ftpServerPath.startsWith("/")) {
			ftpServerPath = StringUtils.substringAfter(ftpServerPath, "/");
		}
		Message<String> message = null;
		try {
			message = MessageBuilder.withPayload(fileName)
					.setHeader("file_remoteDirectory", new String(ftpServerPath.getBytes(Charset.forName("UTF-8")),"ISO-8859-1"))
					.setHeader("file_remoteFile", new String(fileName.getBytes(Charset.forName("UTF-8")),"ISO-8859-1"))
					.build();
		} catch (Exception e) {
			LOG.error("ftp删除图片产生异常",e);
			throw new Exception(e);
		}
		LOG.info("删除ftp图片：getHeaders="+message.getHeaders());
		return ftpRemoveChannel.send(message);
	}
	
}
