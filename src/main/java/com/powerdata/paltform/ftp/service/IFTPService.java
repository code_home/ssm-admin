package com.powerdata.paltform.ftp.service;

import java.io.InputStream;

public interface IFTPService {
	/**
	 * ftp上传
	 * @param in
	 * @param ftpServerPath
	 * @param fileName
	 * @return
	 * @throws Exception 
	 */
	public boolean uploadFile(String ftpServerPath, String fileName, InputStream in) throws Exception;
	
	/**
	 * ftp删除
	 * @param fileName
	 * @param ftpServerPath
	 * @return
	 * @throws Exception 
	 */
	public boolean deleteFile(String fileName, String ftpServerPath) throws Exception;
}
