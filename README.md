#ssmproject
Maven项目
Spring+SpringMVC+Mybatis+Shiro+Redis+Ehcache+Mysql+Jsp+Log4j+PageHelper分页插件=基础框架完整版

技术要点：
1、缓存Redis
2、分布式服务框架Dubbo
3、异步消息中间件Rabbit

SQL文件：doc目录
运行环境：JDK1.7+TOMCAT7
项目运行：
1、直接部署到tomcat运行
2、tomcat插件启动项目，需要在pom.xml中加入tomcat插件，右键项目,run as->maven bulid->输入Goals参数：tomcat:run
3、以jetty插件方式，启动项目，需要在pom.xml中加入jetty插件，然后右键项目->run as->maven build->输入Goals参数：jetty:run
<!-- jetty 启动插件，jetty插件启动项目，更快 -->
			<plugin>
				<groupId>org.mortbay.jetty</groupId>
				<artifactId>jetty-maven-plugin</artifactId>
				<version>8.1.16.v20140903</version>
				<configuration>
					<!-- 注释掉scanIntervalSeconds 可以是实现热部署-->
					<!--  
				    <scanIntervalSeconds>0</scanIntervalSeconds> -->
				    <webAppConfig>
				    	<contextPath>/</contextPath>
				    	<defaultsDescriptor>   ${project.basedir}/src/main/webapp/WEB-INF/webdefault.xml</defaultsDescriptor>
				    </webAppConfig>
				    <classesDirectory>target/classes</classesDirectory>
				    <webAppSourceDirectory>${project.basedir}/src/main/webapp</webAppSourceDirectory>
				    <!-- 修改jetty的默认端口 -->  
					<connectors>  
					    <connector implementation="org.eclipse.jetty.server.nio.SelectChannelConnector">  
					        <port>8090</port>  
					        <maxIdleTime>60000</maxIdleTime>  
					    </connector>  
					</connectors>
				</configuration>
			</plugin>
			