/*
 * 类名 PageData.java
 *
 * 版本信息 
 *
 * 日期 2014年3月21日
 *
 * 版权声明Copyright (C) 2014 YouGou Information Technology Co.,Ltd
 * All Rights Reserved.
 * 本软件为优购科技开发研制，未经本公司正式书面同意，其他任何个人、团体不得
 * 使用、复制、修改或发布本软件。
 */
package com.powerdata.framework.base;

import java.util.List;

/**
 * Class description goes here
 * Created by ylq on 14-5-5 下午5:02.
 */
public class PageData<T> extends Base {

    /**
	 * 
	 */
	private static final long serialVersionUID = 3897065036167737619L;

	/**
     * 每页的记录数
     */
    private int pageSize = 20;

    /**
     * 当前页中存放的数据
     */
    private List<T> data;

    /**
     * 总记录数
     */
    private long rowCount;

    /**
     * 页数
     */
    private int pageCount;

    /**
     * 跳转页数
     */
    private int pageNo;

    /**
     * 是否有上一页
     */
    private boolean hasPrevious = false;

    /**
     * 是否有下一页
     */
    private boolean hasNext = false;

    public PageData(int pageNo, int rowCount) {
        this.pageNo = pageNo;
        this.rowCount = rowCount;
        this.pageCount = getTotalPageCount();
        refresh();
    }

    /**
     * 构造方法
     */
    public PageData(int pageNo, int pageSize, long rowCount) {
        this.pageNo = pageNo;
        this.pageSize = pageSize;
        this.rowCount = rowCount;
        this.pageCount = getTotalPageCount();
        refresh();
    }

    /**
     *
     */
    public PageData(int pageNo, int pageSize, long rowCount, List<T> data) {
        this.pageNo = pageNo;
        this.pageSize = pageSize;
        this.rowCount = rowCount;
        this.pageCount = getTotalPageCount();
        this.data = data;
        refresh();
    }

    /**
     * 取总页数
     */
    private final int getTotalPageCount() {
        if (rowCount % pageSize == 0)
            return (int)(rowCount / pageSize);
        else
            return (int)(rowCount / pageSize) + 1;
    }

    /**
     * 刷新当前分页对象数据
     */
    private void refresh() {
        if (pageCount <= 1) {
            hasPrevious = false;
            hasNext = false;
        } else if (pageNo == 1) {
            hasPrevious = false;
            hasNext = true;
        } else if (pageNo == pageCount) {
            hasPrevious = true;
            hasNext = false;
        } else {
            hasPrevious = true;
            hasNext = true;
        }
    }

    /**
     * 取每页数据数
     */
    public int getPageSize() {
        return pageSize;
    }

    /**
     * 取当前页中的记录.
     */
    public Object getResult() {
        return data;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }

    public long getRowCount() {
        return rowCount;
    }

    public void setRowCount(long rowCount) {
        this.rowCount = rowCount;
    }

    public int getPageCount() {
        return pageCount;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }

    public int getPageNo() {
        return pageNo;
    }

    public void setPageNo(int pageNo) {
        this.pageNo = pageNo;
    }

    public boolean isHasPrevious() {
        return hasPrevious;
    }

    public void setHasPrevious(boolean hasPrevious) {
        this.hasPrevious = hasPrevious;
    }

    public boolean isHasNext() {
        return hasNext;
    }

    public void setHasNext(boolean hasNext) {
        this.hasNext = hasNext;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }

    public int getStartOfPage() {
        return (pageNo - 1) * pageSize;
    }
}
