package com.powerdata.framework.util;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;

/**
 * @author Akim
 *
 */
public class CalculateUtils
{
	private CalculateUtils() {}
	
	/**
	 * 加法
	 * @param one 第一个加数
	 * @param two 第二个加数
	 * @param num 精确位数
	 * @return one+two
	 */
	public static double add(double one, double two, int num) {
	
		BigDecimal bdOne = new BigDecimal(String.valueOf(one));
		BigDecimal bdTwo = new BigDecimal(String.valueOf(two));
		BigDecimal total = bdOne.add(bdTwo);
		
		BigDecimal divisor = new BigDecimal("1");
		BigDecimal result = total.divide(divisor, num, BigDecimal.ROUND_HALF_UP);
		return result.doubleValue();
	}
	/**
	 * 加法
	 * @param one 第一个加数
	 * @param two 第二个加数
	 * @return one+two
	 */
	public static double add(double one, double two) {
		
		BigDecimal bdOne = new BigDecimal(String.valueOf(one));
		BigDecimal bdTwo = new BigDecimal(String.valueOf(two));
		BigDecimal total = bdOne.add(bdTwo);
		
		return total.doubleValue();
	}
	/**
	 * 减法
	 * @param one 被减数
	 * @param two 减数
	 * @param num 精确位数
	 * @return one-two
	 */
	public static double subtract(double one, double two, int num) {
		
		BigDecimal bdOne = new BigDecimal(String.valueOf(one));
		BigDecimal bdTwo = new BigDecimal(String.valueOf(two));
		BigDecimal total = bdOne.subtract(bdTwo);
		
		BigDecimal divisor = new BigDecimal("1");
		BigDecimal result = total.divide(divisor, num, BigDecimal.ROUND_HALF_UP);
		return result.doubleValue();
	}
	/**
	 * 减法
	 * @param one 被减数
	 * @param two 减数
	 * @return one-two
	 */
	public static double subtract(double one, double two) {
		
		BigDecimal bdOne = new BigDecimal(String.valueOf(one));
		BigDecimal bdTwo = new BigDecimal(String.valueOf(two));
		BigDecimal total = bdOne.subtract(bdTwo);
		
		return total.doubleValue();
	}
	
	/**
	 * 乘法
	 * @param one 第一个乘数
	 * @param two 第二个乘数
	 * @param num 精确位数
	 * @return one*two
	 */
	public static double multiply(double one, double two, int num) {
		
		BigDecimal bdOne = new BigDecimal(String.valueOf(one));
		BigDecimal bdTwo = new BigDecimal(String.valueOf(two));
		BigDecimal total = bdOne.multiply(bdTwo);
		
		BigDecimal divisor = new BigDecimal("1");
		BigDecimal result = total.divide(divisor, num, BigDecimal.ROUND_HALF_UP);
		return result.doubleValue();
	}
	/**
	 * 乘法
	 * @param one 第一个乘数
	 * @param two 第二个乘数
	 * @return one*two
	 */
	public static double multiply(double one, double two) {
		
		BigDecimal bdOne = new BigDecimal(String.valueOf(one));
		BigDecimal bdTwo = new BigDecimal(String.valueOf(two));
		BigDecimal total = bdOne.multiply(bdTwo);
		
		return total.doubleValue();
	}
	/**
	 * 除法
	 * @param one 被除数
	 * @param two 除数
	 * @param num 精确位数
	 * @return one/two
	 */
	public static double divide(double one, double two, int num) {
		
		BigDecimal bdOne = new BigDecimal(String.valueOf(one));
		BigDecimal bdTwo = new BigDecimal(String.valueOf(two));
		
		BigDecimal total = bdOne.divide(bdTwo, num, BigDecimal.ROUND_HALF_UP);
		
		return total.doubleValue();
	}

    /**
     *
     * @param one
     * @param two
     * @return
     */
	public static  Integer add (Integer one ,Integer two){
        Integer returnInt = null;
        if(one==null&&two==null){
            return returnInt;
        }else{
            if (one!=null)
                returnInt = one;
            if(two !=null){
                int tempInt = two.intValue();
                if(returnInt!=null)
                    returnInt = returnInt.intValue()+tempInt;
                else
                    returnInt= tempInt;
            }
        }
        return returnInt;
    }
	public static double convertStringToDouble(String d) throws ParseException {
		DecimalFormatSymbols dfs = new DecimalFormatSymbols();  
        dfs.setDecimalSeparator('.');  
        dfs.setGroupingSeparator(',');  
        dfs.setMonetaryDecimalSeparator('.');  
        DecimalFormat df = new DecimalFormat("###,###.##",dfs);  
          
        Number num = df.parse(d);  
        return num.doubleValue(); 
	}

}
