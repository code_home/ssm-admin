package com.powerdata.manage.front;

import javax.annotation.Resource;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import com.powerdata.framework.base.PageModel;
import com.powerdata.framework.base.Query;
import com.powerdata.manage.background.model.User;
import com.powerdata.manage.background.service.IUserService;

@Controller
@RequestMapping("/page")
public class Test {
	private static final Logger LOG = Logger.getLogger(Test.class);
	
	@Resource
	private IUserService userService;
	
	@RequestMapping("/test")
	public String test() {
		LOG.info("测试。。。。。。。。。。。。。");
		return "index";
	}
	
	@RequestMapping("/query")
	@ResponseBody
	public String query(User user) {
		System.out.println("测试...................");
		try {
			PageInfo<User> pageInfo1 = userService.findPageList(user, new PageModel());
//			PageInfo<User> pageInfo2 = userService.queryPageList(user, new Query());
//			Page<User> pageInfo3 = userService.queryPage(user, new Query());
//			PageModel<User> pageInfo4 = userService.findPage(user, new PageModel());
			LOG.info("返回结果列表1：" + pageInfo1+"，列表1："+pageInfo1.getList());
//			LOG.info("返回结果列表2：" + pageInfo2+"，列表2："+pageInfo2.getList());
//			LOG.info("返回结果列表2：" + pageInfo3+"，列表3："+pageInfo3.getResult());
//			LOG.info("返回结果列表3：" + pageInfo4+"，列表4："+pageInfo4.getItems());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "success";
	}
	
}
