package com.powerdata.framework.ftp.impl;

import java.awt.image.BufferedImage;
import java.io.InputStream;

import javax.imageio.ImageIO;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.MessageChannel;
import org.springframework.stereotype.Service;

import com.powerdata.framework.base.Result;
import com.powerdata.framework.ftp.FTPUtil;
import com.powerdata.framework.ftp.FtpConf;
import com.powerdata.framework.ftp.IFtpService;
import com.powerdata.framework.util.SpringContextHolder;
import com.powerdata.framework.util.UUIDGenerator;

@Service
public class FtpServiceImpl implements IFtpService {
	private static final Logger LOG = Logger.getLogger(FtpServiceImpl.class);
	
	@Autowired
	private MessageChannel ftpUploadChannel;
	@Autowired
	private MessageChannel ftpRemoveChannel;
	
	@Override
	public Result<String> uploadFile(InputStream in, FtpConf ftpConf, String name) {
		try {
			if (in == null || StringUtils.isBlank(name)) {
				return Result.create(1, "请选择适合的图片", "");
			}
			if (in.available() > ftpConf.getMaxSize()) {
				int fileSize = ftpConf.getMaxSize()/1024/1024;
				return Result.create(2, String.format("图片不能超过%sM", fileSize), "");
			}
			if (ftpConf.getWidth() > 0 && ftpConf.getHeight() > 0) {
				BufferedImage image = ImageIO.read(in);
				if (ftpConf == FtpConf.SELLER_AUTHORIZE || ftpConf == FtpConf.SELLER_IDENTITY) {
					if (image.getWidth() < ftpConf.getWidth() || image.getHeight() < ftpConf.getHeight()) {
						return Result.create(3, "图片尺寸必须：宽度>="+ftpConf.getWidth()+"，高度>="+ftpConf.getHeight(), "");
					}
				} else {
					if (image.getWidth() != ftpConf.getWidth() || image.getHeight() != ftpConf.getHeight()) {
						return Result.create(4, "图片尺寸必须是："+ftpConf.getWidth()*ftpConf.getHeight(), "");
					}
				}
			} 
		} catch (Exception e) {
			LOG.error("上传图片校验失败！", e);
			return Result.create(0, "上传图片校验失败!", "");
		}
		
		String ftpServerPath = ftpConf.getPath();
		String extend = FilenameUtils.getExtension(name);// 文件扩展名
		String fileName = UUIDGenerator.get32LowCaseUUID().toUpperCase() + "." + extend;
		try {
			boolean uploadFlag = FTPUtil.ftpUpload(ftpUploadChannel,in, fileName, ftpServerPath);
			if (!uploadFlag) {
				return Result.create(10, "上传图片失败，ftp服务器错误!", "");
			}
			return Result.create(11, "上传图片成功!", ftpServerPath+fileName);
		} catch (Exception e) {
			LOG.error("上传图片失败！", e);
			return Result.create(0, "上传图片失败，ftp服务器错误!", "");
		}
	}

	@Override
	public Result<String> deleteFile(String fileName, String ftpServerPath) {
		String path = ftpServerPath + "/" + fileName;
		Result<String> result = null;
		try {
			boolean deleteFlag = FTPUtil.ftpDeleteload(ftpRemoveChannel, fileName, ftpServerPath);
			if (!deleteFlag) {
				result = Result.create(20, "文件【"+path+"】，删除失败！", "");
			}
			result = Result.create(21, "文件【"+path+"】，删除成功！", "");
		} catch (Exception e) {
			LOG.error("删除ftp服务器上的文件出错", e);
			result = Result.create(22, "文件【"+path+"】，删除异常，服务器内部错误！", "");
		}
		return result;
	}

	@Override
	public String obtainImgBaseUrl() {
		String baseUrl = SpringContextHolder.getBean("imageBaseUrl");
		if(StringUtils.isNotBlank(baseUrl)) {
			return baseUrl;
		}
		return "";
	}
	
}
