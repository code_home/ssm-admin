package com.powerdata.manage.background.service;

import java.util.List;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import com.powerdata.framework.base.PageModel;
import com.powerdata.framework.base.Query;
import com.powerdata.framework.pagehelper.PagedResult;
import com.powerdata.manage.background.model.User;

public interface IUserService {
	
	/**
	 * 查找
	 * @throws Exception
	 */
	public User select(User user);
	
	/**
	 * 新增用户
	 */
	public String insert(User user);
	
	/**
	 * 通过id查询数据
	 */
	public User getById(String id);
	
	/**
	 * 查询用户列表
	 * @return
	 * @throws Exception
	 */
	public List<User> queryList(User user);
	
	public PageInfo<User> findPageList(User user, PageModel pageModel);
	
	public PageModel<User> findPage(User user, PageModel pageModel);
	
	public PageInfo<User> queryPageList(User user, Query query);
	
	public Page<User> queryPage(User user, Query query);
	
	public PagedResult<User> queryByPage(User user, Query query);
	
	public int findPageListCount(User user);
	
	/**
	 * 从redis中查询数据
	 * @param user
	 * @return
	 */
	public User queryRedisData(User user);
	
	/**
	 * 更新用户
	 * @param user
	 * @return
	 * @throws Exception
	 */
	public int update(User user);
	
	/**
	 * 删除用户
	 * @param user
	 * @return
	 * @throws Exception
	 */
	public int delete(String id);
}
