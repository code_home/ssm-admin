package com.powerdata.manage.background.job;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 注解方式执行调度
 * @author he.xx
 */
@Component
public class JobAnnotationTest {
	
	public JobAnnotationTest() {
		System.out.println("Job创建成功");
	}
	
	//每隔1秒执行一次
	@Scheduled(cron="0/1 * *  * * ?")
	public void execute() {
		System.out.println("注解---执行调度---------------------------");
	}
	
}
