package com.powerdata.framework.ftp;

import java.io.InputStream;

import com.powerdata.framework.base.Result;

/**
 * Ftp上传Service
 * @author zhang.hc
 *
 */
public interface IFtpService {
	/**
	 * ftp上传图片底层
	 * @param in
	 * @param uploadDir
	 * @param name
	 * @return
	 */
	public Result<String> uploadFile(InputStream in, FtpConf ftpConf, String name);
	
	/**
	 * ftp删除资源
	 * @param fileName
	 * @param ftpServerPath
	 * @return
	 */
	public Result<String> deleteFile(String fileName, String ftpServerPath);
	
	public String obtainImgBaseUrl();
}
