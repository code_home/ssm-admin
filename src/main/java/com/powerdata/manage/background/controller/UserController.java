package com.powerdata.manage.background.controller;

import java.util.Date;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import com.powerdata.framework.base.PageModel;
import com.powerdata.framework.base.Query;
import com.powerdata.framework.pagehelper.PagedResult;
import com.powerdata.framework.util.MD5Builder;
import com.powerdata.manage.background.model.User;
import com.powerdata.manage.background.service.IUserService;

@Controller
@RequestMapping(value="/admin")
public class UserController {
	private static final Logger LOG = LoggerFactory.getLogger(UserController.class);
	
	@Resource
	private IUserService userService;
	
	@RequestMapping("/index")
	public String index() {
		LOG.info("测试测试：{}", "张三");
		System.out.println("测试------------------------------------------");
		return "index";
	}
	
	/*-----------------------------验证Oval框架，限制参数不能为空------------- */
	@RequestMapping("/select1")
	@ResponseBody
	public String select1() {
		User user = new User();
		user.setUsername("hxx3");
		userService.select(user);
		return "select1";
	}
	
	@RequestMapping("/select2")
	@ResponseBody
	public String select2() {
		userService.getById("");
		return "select2";
	}
	
	/*--------------------测试自定义注解，切面记录日志---------------------*/
	@RequestMapping("/queryList")
	@ResponseBody
	public String queryList() {
		userService.queryList(new User());
		return "queryList查询列表，切面记录日志";
	}
	
	/*----------------------验证ehcache缓存------------------------------------------ */
	/**
	 * 获取数据，如果有缓存(ehcache缓存)，则从缓存中取值
	 * @date 2017年1月17日
	 * @param user
	 * @return
	 */
	@RequestMapping("/queryData")
	@ResponseBody
	public String queryData(User user) {
		user.setUsername("hxx2");
		PageModel<User> pageInfo1 = userService.findPage(user, new PageModel());
		PageModel<User> pageInfo2 = userService.findPage(user, new PageModel());
		return JSON.toJSONString(pageInfo1==pageInfo2);
	}
	
	/*------------------------验证redis缓存--------------------------------*/
	/**
	 * 先查询redis缓存数据
	 */
	@RequestMapping("/queryRedisData")
	@ResponseBody
	public String queryRedisData(User user) {
		user.setUsername("hxx2");
		User userInfo = userService.queryRedisData(user);
		return "success";
	}
	
	/*--------------------验证分页-------------------------------------*/
	@RequestMapping("/get")
	@ResponseBody
	public Object get(User user) {
		System.out.println("测试...................");
		PageInfo<User> pageInfo1 = userService.findPageList(user, new PageModel());
		PageInfo<User> pageInfo2 = userService.queryPageList(user, new Query());
		Page<User> pageInfo3 = userService.queryPage(user, new Query());
		PageModel<User> pageInfo4 = userService.findPage(user, new PageModel());
//			LOG.info("返回结果列表1：" + pageInfo1+"，列表1："+pageInfo1.getList().toString()+",json数据:"+JSON.toJSONString(pageInfo1));
//			LOG.info("返回结果列表2：" + pageInfo2+"，列表2："+pageInfo2.getList().toString()+",json数据:"+JSON.toJSONString(pageInfo2));
//			LOG.info("返回结果列表2：" + pageInfo3+"，列表3："+pageInfo3.getResult()+",json数据:"+JSON.toJSONString(pageInfo3));
//			LOG.info("返回结果列表3：" + pageInfo4+"，列表4："+pageInfo4.getItems().toString()+",json数据:"+JSON.toJSONString(pageInfo4));
		return pageInfo4;
	}
	
	@RequestMapping("/get2")
	@ResponseBody
	public String get2(User user) {
		PageModel<User> pageInfo4 = userService.findPage(user, new PageModel());
		return JSON.toJSONString(pageInfo4);
	}
	
	@RequestMapping("/get3")
	@ResponseBody
	public String get3(User user) {
		PageInfo<User> pageInfo1 = userService.findPageList(user, new PageModel());
		return JSON.toJSONString(pageInfo1);
	}
	
	@RequestMapping("/get4")
	@ResponseBody
	public String get4(User user) {
		PageInfo<User> pageInfo2 = userService.queryPageList(user, new Query());
		return JSON.toJSONString(pageInfo2);
	}
	
	@RequestMapping("/get5")
	@ResponseBody
	public String get5(User user) {
		Page<User> pageInfo3 = userService.queryPage(user, new Query());
		return JSON.toJSONString(pageInfo3);
	}
	
	@RequestMapping("/query")
	@ResponseBody
	public String query(User user) {
		System.out.println("测试...................");
		try {
			PageInfo<User> pageInfo1 = userService.findPageList(user, new PageModel());
			PageInfo<User> pageInfo2 = userService.queryPageList(user, new Query());
			Page<User> pageInfo3 = userService.queryPage(user, new Query());
			PageModel<User> pageInfo4 = userService.findPage(user, new PageModel());
			PagedResult<User> pagedResult = userService.queryByPage(user, new Query());
			LOG.info("返回结果列表1：" + pageInfo1+"，列表1："+pageInfo1.getList().toString()+",json数据:"+JSON.toJSONString(pageInfo1));
			LOG.info("返回结果列表2：" + pageInfo2+"，列表2："+pageInfo2.getList().toString()+",json数据:"+JSON.toJSONString(pageInfo2));
			LOG.info("返回结果列表3：" + pageInfo3+"，列表3："+pageInfo3.getResult()+",json数据:"+JSON.toJSONString(pageInfo3));
			LOG.info("返回结果列表4：" + pageInfo4+"，列表4："+pageInfo4.getItems().toString()+",json数据:"+JSON.toJSONString(pageInfo4));
			LOG.info("返回结果列表5：" + pagedResult+",json数据:"+JSON.toJSONString(pagedResult));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "success";
	}
	
	@RequestMapping("/insert")
	public void insert() {
		try {
			for (int i = 36; i < 38; i++) {
				User user = new User();
				user.setCreateTime(new Date());
				user.setType(0);
				user.setSex(0);
				user.setStatus(0);
				user.setUsername("hxx"+i);
				user.setPassword(MD5Builder.getMD5("123456"));
				userService.insert(user);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * 跳转到登录页面
	 * @date 2016年12月23日
	 */
	@RequestMapping("/login")
	public String login(HttpServletRequest request, HttpServletResponse response, Model model) {
		Subject currentUser = SecurityUtils.getSubject();
		if (currentUser != null && currentUser.isAuthenticated()) {
			return "redirect:index.htm";
		}
		return "login";
	}
	
	@RequestMapping(value = "/checkLogin", method = RequestMethod.POST)
	public String checkLogin(HttpServletRequest request, String username, String password) {
		UsernamePasswordToken token = new UsernamePasswordToken(username, MD5Builder.getMD5(password));
		Subject currentUser = SecurityUtils.getSubject();
		try {
			if (!currentUser.isAuthenticated()) {
				token.setRememberMe(true);
				currentUser.login(token);
			}
			return "redirect:index.htm";
		} catch (Exception e) {
			e.printStackTrace();
			return "redirect:login.htm";
		}
	}
	
	/** 退出登录 */
	@RequestMapping(value="/logout")
	public String logout() {
		Subject currentUser = SecurityUtils.getSubject();
		if (currentUser != null) {
			currentUser.logout();
		}
		return "redirect:login.htm";
	}
	
	/** 退出登录 */
	@RequestMapping(value="/error")
	public String error() {
		return "error";
	}
	
}
