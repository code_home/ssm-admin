/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50539
Source Host           : localhost:3306
Source Database       : law_db

Target Server Type    : MYSQL
Target Server Version : 50539
File Encoding         : 65001

Date: 2017-01-12 15:47:26
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tbl_sys_article
-- ----------------------------
DROP TABLE IF EXISTS `tbl_sys_article`;
CREATE TABLE `tbl_sys_article` (
  `id` varchar(32) NOT NULL COMMENT '主键id',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `article_no` int(20) DEFAULT NULL COMMENT '文章编号',
  `article_title` varchar(200) DEFAULT NULL COMMENT '文章标题',
  `article_content` longtext COMMENT '文章内容',
  `article_picture` varchar(50) DEFAULT NULL COMMENT '文章封面',
  `article_type` tinyint(2) DEFAULT '1' COMMENT '文章类型（1:原著，2：其他）',
  `publish_flag` tinyint(2) DEFAULT '1' COMMENT '文章发布状态（1:未发布，2:已发布）',
  `recommend_flag` tinyint(2) DEFAULT '1' COMMENT '推荐状态（1:未推荐，2:已推荐）',
  `delete_flag` tinyint(2) DEFAULT '1' COMMENT '删除状态（1:未删除，2:删除，如果状态为2，则可以在回收站查看）',
  `channel_id` varchar(32) DEFAULT NULL COMMENT '频道id，频道表id',
  `sort_no` int(5) DEFAULT NULL COMMENT '排序号',
  `operator` varchar(32) DEFAULT NULL COMMENT '操作人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `idx_article_no` (`article_no`),
  KEY `idx_article_title` (`article_title`),
  KEY `idx_channel_id` (`channel_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='文章表';

-- ----------------------------
-- Records of tbl_sys_article
-- ----------------------------
INSERT INTO `tbl_sys_article` VALUES ('0', null, null, null, null, null, '1', '1', '1', '1', null, null, null, null);

-- ----------------------------
-- Table structure for tbl_sys_user
-- ----------------------------
DROP TABLE IF EXISTS `tbl_sys_user`;
CREATE TABLE `tbl_sys_user` (
  `id` varchar(32) NOT NULL COMMENT '用户ID',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `username` varchar(50) NOT NULL COMMENT '用户账号',
  `password` varchar(50) NOT NULL COMMENT '用户密码',
  `type` int(2) NOT NULL COMMENT '用户类型（0：超级管理员 1：普通用户）',
  `status` int(2) NOT NULL COMMENT '用户状态(0:启用 1:冻结)',
  `sex` int(2) DEFAULT NULL COMMENT '用户性别(0:男 1:女)',
  `address` varchar(50) DEFAULT NULL COMMENT '用户地址',
  `modify_time` datetime DEFAULT NULL COMMENT '修改时间',
  `login_time` datetime DEFAULT NULL COMMENT '登录时间',
  `is_del` int(2) DEFAULT NULL COMMENT '是否删除 0：未删除 1：删除',
  `note` varchar(200) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_unique_index` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户信息表';

-- ----------------------------
-- Records of tbl_sys_user
-- ----------------------------
INSERT INTO `tbl_sys_user` VALUES ('029b0cc3de1d4495a2523c70c3266993', '2017-01-12 15:38:46', 'hxx0', 'e10adc3949ba59abbe56e057f20f883e', '0', '0', '0', null, null, null, null, null);
INSERT INTO `tbl_sys_user` VALUES ('04f17800e35a49f4a38c5034f1c58fe3', '2017-01-12 15:38:48', 'hxx29', 'e10adc3949ba59abbe56e057f20f883e', '0', '0', '0', null, null, null, null, null);
INSERT INTO `tbl_sys_user` VALUES ('0c4a0a41021945e8ac13636484e34505', '2017-01-12 15:38:48', 'hxx21', 'e10adc3949ba59abbe56e057f20f883e', '0', '0', '0', null, null, null, null, null);
INSERT INTO `tbl_sys_user` VALUES ('12653a9e2c5f444e9e800d204f0d875c', '2017-01-12 15:38:47', 'hxx19', 'e10adc3949ba59abbe56e057f20f883e', '0', '0', '0', null, null, null, null, null);
INSERT INTO `tbl_sys_user` VALUES ('17496c50706c47afad45189e59873538', '2017-01-12 15:38:47', 'hxx14', 'e10adc3949ba59abbe56e057f20f883e', '0', '0', '0', null, null, null, null, null);
INSERT INTO `tbl_sys_user` VALUES ('1c1be6043b844290926d07ae6d9ccf0f', '2017-01-12 15:38:47', 'hxx5', 'e10adc3949ba59abbe56e057f20f883e', '0', '0', '0', null, null, null, null, null);
INSERT INTO `tbl_sys_user` VALUES ('29f3fb99fdb047158a999af5b390957b', '2017-01-12 15:38:48', 'hxx23', 'e10adc3949ba59abbe56e057f20f883e', '0', '0', '0', null, null, null, null, null);
INSERT INTO `tbl_sys_user` VALUES ('379c31f974ec4b2d8c702d2d939d0562', '2017-01-12 15:38:47', 'hxx2', 'e10adc3949ba59abbe56e057f20f883e', '0', '0', '0', null, null, null, null, null);
INSERT INTO `tbl_sys_user` VALUES ('45628c41c9214ea9a7557ae67460b3e2', '2017-01-12 15:38:47', 'hxx4', 'e10adc3949ba59abbe56e057f20f883e', '0', '0', '0', null, null, null, null, null);
INSERT INTO `tbl_sys_user` VALUES ('4efc1c5cce1b4106ae96d61a327c449e', '2017-01-12 15:38:47', 'hxx9', 'e10adc3949ba59abbe56e057f20f883e', '0', '0', '0', null, null, null, null, null);
INSERT INTO `tbl_sys_user` VALUES ('5328655667214c7db301b0aaf7e2e844', '2017-01-12 15:38:47', 'hxx7', 'e10adc3949ba59abbe56e057f20f883e', '0', '0', '0', null, null, null, null, null);
INSERT INTO `tbl_sys_user` VALUES ('5bd4ce700d29441aa8e5864b7127b90d', '2017-01-12 15:38:48', 'hxx26', 'e10adc3949ba59abbe56e057f20f883e', '0', '0', '0', null, null, null, null, null);
INSERT INTO `tbl_sys_user` VALUES ('5f428230d1e7463ba08353a7242c12da', '2017-01-12 15:38:47', 'hxx11', 'e10adc3949ba59abbe56e057f20f883e', '0', '0', '0', null, null, null, null, null);
INSERT INTO `tbl_sys_user` VALUES ('60028b5b33a3452cb84cf049fdc54da4', '2017-01-12 15:38:48', 'hxx20', 'e10adc3949ba59abbe56e057f20f883e', '0', '0', '0', null, null, null, null, null);
INSERT INTO `tbl_sys_user` VALUES ('647e2b174f2649adb18e75237fc94b41', '2017-01-12 15:38:48', 'hxx25', 'e10adc3949ba59abbe56e057f20f883e', '0', '0', '0', null, null, null, null, null);
INSERT INTO `tbl_sys_user` VALUES ('6bb78c1b6f2d4562ba21c2450c524696', '2017-01-12 15:38:47', 'hxx18', 'e10adc3949ba59abbe56e057f20f883e', '0', '0', '0', null, null, null, null, null);
INSERT INTO `tbl_sys_user` VALUES ('6bcbb35b2e164416b6142d4477f98b42', '2017-01-12 15:38:47', 'hxx16', 'e10adc3949ba59abbe56e057f20f883e', '0', '0', '0', null, null, null, null, null);
INSERT INTO `tbl_sys_user` VALUES ('6c3c0d1710f8458b896d23b9f0cfe795', '2017-01-12 15:38:47', 'hxx10', 'e10adc3949ba59abbe56e057f20f883e', '0', '0', '0', null, null, null, null, null);
INSERT INTO `tbl_sys_user` VALUES ('73ade1e4af194c6eb701f0936196636f', '2017-01-12 15:38:47', 'hxx12', 'e10adc3949ba59abbe56e057f20f883e', '0', '0', '0', null, null, null, null, null);
INSERT INTO `tbl_sys_user` VALUES ('7c7afe4722794465b29cbe1db09f84dc', '2017-01-12 15:38:47', 'hxx17', 'e10adc3949ba59abbe56e057f20f883e', '0', '0', '0', null, null, null, null, null);
INSERT INTO `tbl_sys_user` VALUES ('7ff3946f03714c2cbe9ee8bdf6f71b04', '2017-01-12 15:38:48', 'hxx33', 'e10adc3949ba59abbe56e057f20f883e', '0', '0', '0', null, null, null, null, null);
INSERT INTO `tbl_sys_user` VALUES ('81fc2bfc21bd42bd85df1f83fcd51e53', '2017-01-12 15:38:47', 'hxx1', 'e10adc3949ba59abbe56e057f20f883e', '0', '0', '0', null, null, null, null, null);
INSERT INTO `tbl_sys_user` VALUES ('840e1307201b43a0bbc6ce073baea2fc', '2017-01-12 15:38:47', 'hxx8', 'e10adc3949ba59abbe56e057f20f883e', '0', '0', '0', null, null, null, null, null);
INSERT INTO `tbl_sys_user` VALUES ('9d35dbe9349543b980f412a2c0ca1b8d', '2017-01-12 15:38:48', 'hxx27', 'e10adc3949ba59abbe56e057f20f883e', '0', '0', '0', null, null, null, null, null);
INSERT INTO `tbl_sys_user` VALUES ('a3fce19af44848548cf0c8f4ba06bbc5', '2017-01-12 15:38:47', 'hxx15', 'e10adc3949ba59abbe56e057f20f883e', '0', '0', '0', null, null, null, null, null);
INSERT INTO `tbl_sys_user` VALUES ('a85c93a1e97341a5b2804b6178888f06', '2017-01-12 15:38:48', 'hxx30', 'e10adc3949ba59abbe56e057f20f883e', '0', '0', '0', null, null, null, null, null);
INSERT INTO `tbl_sys_user` VALUES ('ac7769bcb6794035bd44a4a3a1386014', '2016-09-14 17:31:56', 'hexiaoxiong@sina.com', '79a33edd04a5b5749ea03b6283c6fdc2', '0', '0', null, null, null, null, null, null);
INSERT INTO `tbl_sys_user` VALUES ('b0044bded104411ab5a0532e5949d663', '2017-01-12 15:38:47', 'hxx13', 'e10adc3949ba59abbe56e057f20f883e', '0', '0', '0', null, null, null, null, null);
INSERT INTO `tbl_sys_user` VALUES ('b0942e1623e845e3994682276efa2864', '2017-01-12 15:38:47', 'hxx6', 'e10adc3949ba59abbe56e057f20f883e', '0', '0', '0', null, null, null, null, null);
INSERT INTO `tbl_sys_user` VALUES ('b80d7d4d464b47e984f97cf0092fcbf3', '2017-01-12 15:38:48', 'hxx22', 'e10adc3949ba59abbe56e057f20f883e', '0', '0', '0', null, null, null, null, null);
INSERT INTO `tbl_sys_user` VALUES ('c59972df2bb2435ca5c52969e5d38df3', '2017-01-12 15:38:48', 'hxx24', 'e10adc3949ba59abbe56e057f20f883e', '0', '0', '0', null, null, null, null, null);
INSERT INTO `tbl_sys_user` VALUES ('cc376920de094f9cb15c466d21e281d8', '2017-01-12 15:38:48', 'hxx32', 'e10adc3949ba59abbe56e057f20f883e', '0', '0', '0', null, null, null, null, null);
INSERT INTO `tbl_sys_user` VALUES ('d2ab451fee2c4423b8e20521264c6cda', '2016-09-14 17:31:57', 'hexiaoxiong126', '79a33edd04a5b5749ea03b6283c6fdc2', '0', '0', null, null, null, null, null, null);
INSERT INTO `tbl_sys_user` VALUES ('dd3b7c886dd344269d7ec03f31717af3', '2017-01-12 15:38:48', 'hxx34', 'e10adc3949ba59abbe56e057f20f883e', '0', '0', '0', null, null, null, null, null);
INSERT INTO `tbl_sys_user` VALUES ('e35a3e1a968e442cb890e32048db6cb6', '2017-01-12 15:38:48', 'hxx28', 'e10adc3949ba59abbe56e057f20f883e', '0', '0', '0', null, null, null, null, null);
INSERT INTO `tbl_sys_user` VALUES ('fc4f69ddfbf14b20920b951c9c74ec92', '2017-01-12 15:38:47', 'hxx3', 'e10adc3949ba59abbe56e057f20f883e', '0', '0', '0', null, null, null, null, null);
INSERT INTO `tbl_sys_user` VALUES ('ff7d6a45636349b28eea3c7f368487af', '2017-01-12 15:38:48', 'hxx31', 'e10adc3949ba59abbe56e057f20f883e', '0', '0', '0', null, null, null, null, null);
