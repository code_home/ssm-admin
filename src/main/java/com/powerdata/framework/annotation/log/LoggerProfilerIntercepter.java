package com.powerdata.framework.annotation.log;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSON;

/**
 * 使用AOP对方法进行日志记录
 */
@Aspect
@Component
public class LoggerProfilerIntercepter {
	private static final Logger LOG = LoggerFactory.getLogger(LoggerProfilerIntercepter.class);
	
	@Pointcut("@annotation(com.powerdata.framework.annotation.log.LoggerProfile)")
	public void loggerProfilerPointcut() {}
	
	@Around(value="loggerProfilerPointcut()")
	public Object doLoggerProfiler(final ProceedingJoinPoint joinPoint) throws Throwable {
		long startTime = System.currentTimeMillis();
		Map<String, Object> logMap = new HashMap<String, Object>();
		try {
			if (LOG.isInfoEnabled()) {
				Method method = this.getMethod(joinPoint);
				LoggerProfile loggerProfile = method.getAnnotation(LoggerProfile.class);
				if (loggerProfile.methodNote() != null && loggerProfile.methodNote().length() > 0) {
					logMap.put("note", loggerProfile.methodNote());
				}
				String methodName = joinPoint.getSignature().getName();
				String className = joinPoint.getTarget().getClass().getName();
				logMap.put("class", className);
				logMap.put("method", methodName);
				Object[] args = joinPoint.getArgs();
				if (args != null && args.length > 0) {
					for (int i = 0; i < args.length; i++) {
						logMap.put("arg-" + i, args[i]);
					}
				}
				/*
				try {
					//记录dubbo调用信息
					//RpcContext是一个临时状态记录器，当接收到RPC请求，或发起RPC请求时，RpcContext的状态都会变化。 
					//比如：A调B，B再调C，则B机器上，在B调C之前，RpcContext记录的是A调B的信息，在B调C之后，RpcContext记录的是B调C的信息。
					RpcContext rpcContext = RpcContext.getContext();
					if (rpcContext != null) {
						logMap.put("remoteAddress",rpcContext.getRemoteAddressString());
						URL url = rpcContext.getUrl();
						if (url != null) {
							logMap.put("application",url.getParameter("application"));
						}
					}
				} catch (Exception e) {
					LOG.error("RpcContext.getContext() Exception ", e);
				} */
			}
		} catch (Exception e) {
			LOG.error("LoggerProfile Exception ", e);
		}
		
		Object obj = null;
		Exception error = null;
		try {
			obj = joinPoint.proceed();
		} catch (Exception ex) {
			error = ex;
		}
		try {
			if (LOG.isInfoEnabled()) {
				if (error != null) {
					logMap.put("exception", error.getMessage());
					logMap.put("result", null);
				} else {
					logMap.put("result", obj == null?"null":obj);
				}
				long endTime = System.currentTimeMillis();
				logMap.put("time", String.valueOf((endTime - startTime)) + "ms");
				LOG.info(JSON.toJSONString(logMap));
			}
		}catch(Exception ex){
			LOG.error("LoggerProfile Exception ", ex);
		}
		if (error != null) {
			throw error;
		}
		return obj;
	}
	
	/**
	 * @param joinPoint
	 * @return
	 * @throws NoSuchMethodException
	 */
	protected Method getMethod(final JoinPoint joinPoint) throws NoSuchMethodException {
		final Signature sig = joinPoint.getSignature();
		if (!(sig instanceof MethodSignature)) {
			throw new NoSuchMethodException("This annotation is only valid on a method.");
		}
		final MethodSignature msig = (MethodSignature) sig;
		final Object target = joinPoint.getTarget();
		return target.getClass().getMethod(msig.getName(), msig.getParameterTypes());
	}
	
}
